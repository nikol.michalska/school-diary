package com.nikol.michalska.schoolDiary.service;

import com.nikol.michalska.schoolDiary.model.DTO.NewStudentDTO;
import com.nikol.michalska.schoolDiary.model.DTO.StudentDTO;
import com.nikol.michalska.schoolDiary.model.entity.GradesPerLesson;
import com.nikol.michalska.schoolDiary.model.entity.SchoolClass;
import com.nikol.michalska.schoolDiary.model.entity.Student;
import com.nikol.michalska.schoolDiary.model.mappers.StudentMapper;
import com.nikol.michalska.schoolDiary.repository.GradesPerLessonRepository;
import com.nikol.michalska.schoolDiary.repository.SchoolClassRepository;
import com.nikol.michalska.schoolDiary.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.UUID;

@Service
public class StudentService {
    private StudentRepository studentRepository;

    private GradesPerLessonRepository gradesPerLessonRepository;

    private SchoolClassRepository schoolClassRepository;

    private StudentMapper studentMapper = new StudentMapper();

    private SchoolClassService schoolClassService;

    @Autowired
    public StudentService(StudentRepository studentRepository,
                          GradesPerLessonRepository gradesPerLessonRepository,
                          SchoolClassRepository schoolClassRepository,
                          SchoolClassService schoolClassService) {
        this.studentRepository = studentRepository;
        this.gradesPerLessonRepository = gradesPerLessonRepository;
        this.schoolClassRepository = schoolClassRepository;
        this.schoolClassService = schoolClassService;
    }

    public List<StudentDTO> getListOfStudentsDTO(String className) {
        List<Student> listOfStudents = studentRepository.findAllBySchoolClass_NameIgnoreCase(className);
        return studentMapper.mapStudents(listOfStudents);
    }

    public Float getStudentAverage(UUID studentId, String lessonName) {
        GradesPerLesson gradesPerLesson = gradesPerLessonRepository.findByStudent_IdAndSchoolLesson_Name(studentId, lessonName);
        String[] studentGrades = gradesPerLesson.getGrades().split(" ");
        float grades = 0;
        for (int a = 0; a < studentGrades.length; a++) {
            grades = grades + Integer.parseInt(studentGrades[a]);
        }
        Float average = grades / studentGrades.length;
        return average;
    }

    public StudentDTO addNewStudent(NewStudentDTO newStudentDTO) {
        Student student = new Student();
        student.setName(newStudentDTO.getName());
        student.setLastname(newStudentDTO.getLastName());

        SchoolClass schoolClass = schoolClassRepository.findByIdOrThrow(newStudentDTO.getSchoolClassId());

        student.setSchoolClass(schoolClass);
        Student savedStudent = studentRepository.save(student);

        schoolClass.getListOfStudents().add(savedStudent);
        schoolClass.assignStudentNumbers();
        schoolClassRepository.save(schoolClass);
        StudentDTO studentDTO = studentMapper.mapStudent(savedStudent);
        return studentDTO;
    }

    public void moveStudentToAnotherClass(UUID studentId, UUID schoolClassId) {
        Student foundedStudent = studentRepository.findByIdOrThrow(studentId);
        SchoolClass foundedSchoolClass = schoolClassRepository.findByIdOrThrow(schoolClassId);

        foundedStudent.setSchoolClass(foundedSchoolClass);
        foundedSchoolClass.getListOfStudents().add(foundedStudent);
        foundedSchoolClass.assignStudentNumbers();
        schoolClassRepository.save(foundedSchoolClass);
    }

    public List<StudentDTO> getListOfAllStudents() {

        List<Student> listOfStudents = studentRepository.findAll();
        return studentMapper.mapStudents(listOfStudents);
    }

    public Long getNumberOfAllStudents() {
        Long numberOfAllStudents = studentRepository.count();
        return numberOfAllStudents;
    }

    public Long getAverageNumberOfStudentsInClass() {
        Long allStudents = getNumberOfAllStudents();
        Long allSchoolClasses = schoolClassService.getNumberOfAllSchoolClasses();
        return allStudents / allSchoolClasses;
    }

}
