package com.nikol.michalska.schoolDiary.service;

import com.nikol.michalska.schoolDiary.model.entity.SchoolLesson;
import com.nikol.michalska.schoolDiary.repository.SchoolLessonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SchoolLessonService {
    private SchoolLessonRepository schoolLessonRepository;

    @Autowired
    public SchoolLessonService(SchoolLessonRepository schoolLessonRepository) {
        this.schoolLessonRepository = schoolLessonRepository;
    }

    public List<String> getListOfSchoolLessonNames() {
        List<SchoolLesson> listOfSchoolLessons = schoolLessonRepository.findAll();
        List<String> listOfLessonsNames = new ArrayList<>();
        for (int a = 0; a < listOfSchoolLessons.size(); a++) {
            String lesson = listOfSchoolLessons.get(a).getName();
            listOfLessonsNames.add(lesson);
        }
        return listOfLessonsNames;
    }

    public SchoolLesson createNewSchoolLesson(String schoolLessonName) {
        SchoolLesson newSchoolLesson = new SchoolLesson();
        newSchoolLesson.setName(schoolLessonName);
        SchoolLesson savedSchoolLesson = schoolLessonRepository.save(newSchoolLesson);
        return savedSchoolLesson;
    }
}
