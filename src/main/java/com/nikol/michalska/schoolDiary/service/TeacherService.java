package com.nikol.michalska.schoolDiary.service;

import com.nikol.michalska.schoolDiary.model.DTO.NewTeacherDTO;
import com.nikol.michalska.schoolDiary.model.DTO.TeacherDTO;
import com.nikol.michalska.schoolDiary.model.entity.SchoolClass;
import com.nikol.michalska.schoolDiary.model.entity.SchoolLesson;
import com.nikol.michalska.schoolDiary.model.entity.Teacher;
import com.nikol.michalska.schoolDiary.model.mappers.TeacherMapper;
import com.nikol.michalska.schoolDiary.repository.SchoolClassRepository;
import com.nikol.michalska.schoolDiary.repository.SchoolLessonRepository;
import com.nikol.michalska.schoolDiary.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.UUID;

@Service
public class TeacherService {

    private SchoolClassRepository schoolClassRepository;
    private TeacherMapper teacherMapper = new TeacherMapper();

    private TeacherRepository teacherRepository;

    private SchoolLessonRepository schoolLessonRepository;

    @Autowired
    public TeacherService(SchoolClassRepository schoolClassRepository, TeacherRepository teacherRepository,
                          SchoolLessonRepository schoolLessonRepository) {
        this.schoolClassRepository = schoolClassRepository;
        this.teacherRepository = teacherRepository;
        this.schoolLessonRepository = schoolLessonRepository;
    }

    public TeacherDTO getTeacherDTO(String className) {
        SchoolClass schoolClass = schoolClassRepository.findByName(className);
        Teacher teacher = schoolClass.getTeacher();
        TeacherDTO teacherDTO = teacherMapper.mapTeacher(teacher);
        return teacherDTO;
    }

    public TeacherDTO updateTeacher(TeacherDTO teacherDTO) {
        Teacher teacher = teacherRepository.findByIdOrThrow(teacherDTO.getId());
        teacher.setName(teacherDTO.getName());
        teacher.setLastname(teacherDTO.getLastname());
        Teacher savedTeacher = teacherRepository.save(teacher);
        return teacherMapper.mapTeacher(savedTeacher);
    }

    public TeacherDTO createNewTeacher(NewTeacherDTO teacher){
        Teacher newTeacher = new Teacher();
        newTeacher.setName(teacher.getName());
        newTeacher.setLastname(teacher.getLastName());
        newTeacher.setListOfSchoolLessons(new ArrayList<>());

        for (int a = 0; a < teacher.getSchoolLessonsId().size(); a++) {
            UUID lessonToFindId = teacher.getSchoolLessonsId().get(a);
            SchoolLesson schoolLesson = schoolLessonRepository.findByIdOrThrow(lessonToFindId);
            schoolLesson.setTeacher(newTeacher);
            newTeacher.getListOfSchoolLessons().add(schoolLesson);
        }
        Teacher savedTeacher = teacherRepository.save(newTeacher);
        TeacherDTO teacherDTO = teacherMapper.mapTeacher(savedTeacher);
        return teacherDTO;
    }
}
