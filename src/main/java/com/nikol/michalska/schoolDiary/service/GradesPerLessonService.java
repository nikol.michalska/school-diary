package com.nikol.michalska.schoolDiary.service;

import com.nikol.michalska.schoolDiary.model.DTO.AddStudentGradeDTO;
import com.nikol.michalska.schoolDiary.model.entity.GradesPerLesson;
import com.nikol.michalska.schoolDiary.model.entity.SchoolLesson;
import com.nikol.michalska.schoolDiary.model.entity.Student;
import com.nikol.michalska.schoolDiary.repository.GradesPerLessonRepository;
import com.nikol.michalska.schoolDiary.repository.SchoolLessonRepository;
import com.nikol.michalska.schoolDiary.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.UUID;

@Service
public class GradesPerLessonService {
    private StudentRepository studentRepository;

    private SchoolLessonRepository schoolLessonRepository;

    private GradesPerLessonRepository gradesPerLessonRepository;


    @Autowired
    public GradesPerLessonService(StudentRepository studentRepository,
                                  SchoolLessonRepository schoolLessonRepository,
                                  GradesPerLessonRepository gradesPerLessonRepository) {
        this.studentRepository = studentRepository;
        this.schoolLessonRepository = schoolLessonRepository;
        this.gradesPerLessonRepository = gradesPerLessonRepository;
    }

    public GradesPerLesson createNewGradesPerLesson(UUID studentId, UUID schoolLessonId) {
        GradesPerLesson gradesPerLesson = new GradesPerLesson();
        Student foundedStudent = studentRepository.findByIdOrThrow(studentId);
        gradesPerLesson.setStudent(foundedStudent);
        SchoolLesson foundedSchoolLesson = schoolLessonRepository.findByIdOrThrow(schoolLessonId);
        gradesPerLesson.setSchoolLesson(foundedSchoolLesson);
        gradesPerLesson.setGrades("");
        return gradesPerLesson;
    }

    public void updateGrades(AddStudentGradeDTO addStudentGrade) {
        GradesPerLesson gradesPerLesson = gradesPerLessonRepository.findByStudent_IdAndSchoolLesson_Id(addStudentGrade.getStudentId(),
                        addStudentGrade.getSchoolLessonId())
                .orElse(createNewGradesPerLesson(addStudentGrade.getStudentId(), addStudentGrade.getSchoolLessonId()));
        gradesPerLesson.addGrade(addStudentGrade.getGrade());
        gradesPerLessonRepository.save(gradesPerLesson);
    }
}
