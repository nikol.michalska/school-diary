package com.nikol.michalska.schoolDiary.service;

import com.nikol.michalska.schoolDiary.model.DTO.SchoolClassAddStudentsDTO;
import com.nikol.michalska.schoolDiary.model.DTO.SchoolClassDTO;
import com.nikol.michalska.schoolDiary.model.entity.SchoolClass;
import com.nikol.michalska.schoolDiary.model.entity.Student;
import com.nikol.michalska.schoolDiary.model.mappers.SchoolClassMapper;
import com.nikol.michalska.schoolDiary.repository.SchoolClassRepository;
import com.nikol.michalska.schoolDiary.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class SchoolClassService {

    private SchoolClassRepository schoolClassRepository;

    private StudentRepository studentRepository;

    private SchoolClassMapper schoolClassMapper = new SchoolClassMapper();

    @Autowired
    public SchoolClassService(SchoolClassRepository schoolClassRepository,
                              StudentRepository studentRepository) {
        this.schoolClassRepository = schoolClassRepository;
        this.studentRepository = studentRepository;
    }

    public List<SchoolClassDTO> getListOfSchoolClassDTO() {
        List<SchoolClass> listOfSchoolClasses = schoolClassRepository.findAll();
        SchoolClassMapper schoolClassMapper = new SchoolClassMapper();
        List<SchoolClassDTO> listOfSchoolClassesDTO = new ArrayList<>();

        for (int a = 0; a < listOfSchoolClasses.size(); a++) {
            SchoolClassDTO schoolClassDTO = schoolClassMapper.mapSchoolClass(listOfSchoolClasses.get(a));
            listOfSchoolClassesDTO.add(schoolClassDTO);
        }

        return listOfSchoolClassesDTO;
    }

    public SchoolClass createNewSchoolClass(String schoolClassName) {
        SchoolClass schoolClass = new SchoolClass();
        schoolClass.setName(schoolClassName);
        return schoolClassRepository.save(schoolClass);
    }

    public SchoolClassDTO assignStudentsToClass(SchoolClassAddStudentsDTO schoolClassAddStudentsDTO) {
        SchoolClass schoolClass = schoolClassRepository.findByIdOrThrow(schoolClassAddStudentsDTO.getSchoolClassId());

        for (int a = 0; a < schoolClassAddStudentsDTO.getListOfStudents().size(); a++) {
            UUID studentId = schoolClassAddStudentsDTO.getListOfStudents().get(a);
            Student student = studentRepository.findByIdOrThrow(studentId);
            schoolClass.getListOfStudents().add(student);
            student.setSchoolClass(schoolClass);
        }
        schoolClass.assignStudentNumbers();
        SchoolClass savedSchoolClass = schoolClassRepository.save(schoolClass);
        SchoolClassDTO schoolClassDTO = schoolClassMapper.mapSchoolClass(savedSchoolClass);
        return schoolClassDTO;
    }

    public Long getNumberOfAllSchoolClasses() {
        Long numberOfSchoolClasses = schoolClassRepository.count();
        return numberOfSchoolClasses;
    }
}
