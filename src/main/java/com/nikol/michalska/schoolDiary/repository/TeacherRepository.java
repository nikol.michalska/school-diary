package com.nikol.michalska.schoolDiary.repository;

import com.nikol.michalska.schoolDiary.model.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.EntityNotFoundException;
import java.util.UUID;

public interface TeacherRepository extends JpaRepository<Teacher, UUID> {

    default Teacher findByIdOrThrow(UUID id) {
        return findById(id).orElseThrow(() -> new EntityNotFoundException("There is no such teacher: " + id));
    }
}
