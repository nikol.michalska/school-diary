package com.nikol.michalska.schoolDiary.repository;

import com.nikol.michalska.schoolDiary.model.entity.SchoolLesson;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.EntityNotFoundException;
import java.util.UUID;

public interface SchoolLessonRepository extends JpaRepository<SchoolLesson, UUID> {

    default SchoolLesson findByIdOrThrow(UUID id) {
        return findById(id).orElseThrow(() -> new EntityNotFoundException("There is no such school class " + id));
    }
}
