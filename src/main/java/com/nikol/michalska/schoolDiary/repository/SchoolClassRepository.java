package com.nikol.michalska.schoolDiary.repository;

import com.nikol.michalska.schoolDiary.model.entity.SchoolClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.UUID;

@Repository
public interface SchoolClassRepository extends JpaRepository<SchoolClass, UUID> {

    SchoolClass findByName(String name);

    default SchoolClass findByIdOrThrow(UUID id){
        return findById(id).orElseThrow(() -> new EntityNotFoundException("There is no such school class: " + id));
    }
}
