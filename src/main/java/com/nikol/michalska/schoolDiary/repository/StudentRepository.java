package com.nikol.michalska.schoolDiary.repository;

import com.nikol.michalska.schoolDiary.model.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.UUID;

@Repository
public interface StudentRepository extends JpaRepository<Student, UUID> {
    List<Student> findAllBySchoolClass_NameIgnoreCase(String className);

    Student findByNumberAndSchoolClass_NameIgnoreCase(int number, String schoolClass_name);

    default Student findByIdOrThrow(UUID id){
        return findById(id).orElseThrow(()-> new EntityNotFoundException("There is no such student " + id));
    }

}
