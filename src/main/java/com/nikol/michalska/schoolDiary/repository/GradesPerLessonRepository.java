package com.nikol.michalska.schoolDiary.repository;

import com.nikol.michalska.schoolDiary.model.entity.GradesPerLesson;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface GradesPerLessonRepository extends JpaRepository<GradesPerLesson, UUID> {
     GradesPerLesson findByStudent_IdAndSchoolLesson_Name(UUID student_id, String schoolLesson_name);

     Optional<GradesPerLesson> findByStudent_IdAndSchoolLesson_Id(UUID student_id, UUID schoolLesson_id);
}
