package com.nikol.michalska.schoolDiary.controller;

import com.nikol.michalska.schoolDiary.model.DTO.NewTeacherDTO;
import com.nikol.michalska.schoolDiary.model.DTO.TeacherDTO;
import com.nikol.michalska.schoolDiary.model.DTO.TeacherWithLessonsDTO;
import com.nikol.michalska.schoolDiary.model.entity.Teacher;
import com.nikol.michalska.schoolDiary.model.mappers.TeacherMapper;
import com.nikol.michalska.schoolDiary.repository.SchoolClassRepository;
import com.nikol.michalska.schoolDiary.repository.SchoolLessonRepository;
import com.nikol.michalska.schoolDiary.repository.TeacherRepository;
import com.nikol.michalska.schoolDiary.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
public class TeacherController {

    private TeacherRepository teacherRepository;
    private TeacherMapper teacherMapper = new TeacherMapper();

    private SchoolClassRepository schoolClassRepository;

    private TeacherService teacherService;

    private SchoolLessonRepository schoolLessonRepository;

    @Autowired
    public TeacherController(TeacherRepository teacherRepository,
                             SchoolClassRepository schoolClassRepository,
                             TeacherService teacherService,
                             SchoolLessonRepository schoolLessonRepository) {
        this.teacherRepository = teacherRepository;
        this.schoolClassRepository = schoolClassRepository;
        this.teacherService = teacherService;
        this.schoolLessonRepository = schoolLessonRepository;
    }

    @GetMapping(path = "api/teacher/get-all-teachers")
    public ResponseEntity<List<TeacherWithLessonsDTO>> getTeachersWithLesson() {
        List<Teacher> listOfTeachers = teacherRepository.findAll();
        List<TeacherWithLessonsDTO> teacherWithLessonsDTOList = new ArrayList<>();
        for (int a = 0; a < listOfTeachers.size(); a++) {
            Teacher teacher = listOfTeachers.get(a);
            TeacherWithLessonsDTO teacherWithLessonsDTO = teacherMapper.mapTeacherWithLessons(teacher);
            teacherWithLessonsDTOList.add(teacherWithLessonsDTO);

        }
        return ResponseEntity.ok(teacherWithLessonsDTOList);

    }

    @GetMapping(path = "api/teacher/find-by-className/{className}")
    public ResponseEntity<TeacherDTO> getTeacherByClassName(@PathVariable String className) {
        TeacherDTO teacherDTO = teacherService.getTeacherDTO(className);
        return ResponseEntity.ok(teacherDTO);
    }

    @PostMapping(path = "api/teacher/create_new")
    public ResponseEntity<Teacher> createTeacher(@Valid @RequestBody Teacher teacher) {
        Teacher savedTeacher = teacherRepository.save(teacher);
        return ResponseEntity.ok(savedTeacher);
    }

    @PutMapping(path = "api/teacher/update")
    public ResponseEntity<TeacherDTO> updateTeacher(@RequestBody @Valid TeacherDTO teacherDTO) {
        TeacherDTO savedTeacherDTO = teacherService.updateTeacher(teacherDTO);
        return ResponseEntity.ok(savedTeacherDTO);
    }

    @PostMapping(path = "api/teacher/create")
    public ResponseEntity<TeacherDTO> createNewTeacher(@Valid @RequestBody NewTeacherDTO teacher) {
        TeacherDTO teacherDTO = teacherService.createNewTeacher(teacher);
        return ResponseEntity.ok(teacherDTO);
    }

    @DeleteMapping(path = "api/teacher/delete/{teacherId}")
    public void deleteTeacher(@PathVariable UUID teacherId) {
        teacherRepository.deleteById(teacherId);
    }

    @GetMapping(path = "api/teacher/count_all")
    public ResponseEntity<Long> countAllTeachers() {
        Long numberOfAllTeachers = teacherRepository.count();
        return ResponseEntity.ok(numberOfAllTeachers);
    }

}
