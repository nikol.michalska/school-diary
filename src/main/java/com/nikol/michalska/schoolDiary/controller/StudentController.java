package com.nikol.michalska.schoolDiary.controller;

import com.nikol.michalska.schoolDiary.model.DTO.AddStudentGradeDTO;
import com.nikol.michalska.schoolDiary.model.DTO.NewStudentDTO;
import com.nikol.michalska.schoolDiary.model.DTO.StudentDTO;
import com.nikol.michalska.schoolDiary.model.DTO.StudentWithGradesDTO;
import com.nikol.michalska.schoolDiary.model.entity.Student;
import com.nikol.michalska.schoolDiary.model.mappers.StudentMapper;
import com.nikol.michalska.schoolDiary.repository.GradesPerLessonRepository;
import com.nikol.michalska.schoolDiary.repository.SchoolClassRepository;
import com.nikol.michalska.schoolDiary.repository.SchoolLessonRepository;
import com.nikol.michalska.schoolDiary.repository.StudentRepository;
import com.nikol.michalska.schoolDiary.service.GradesPerLessonService;
import com.nikol.michalska.schoolDiary.service.SchoolClassService;
import com.nikol.michalska.schoolDiary.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@RestController
public class StudentController {
    private StudentRepository studentRepository;
    private StudentService studentService;

    private GradesPerLessonRepository gradesPerLessonRepository;

    private SchoolLessonRepository schoolLessonRepository;

    private GradesPerLessonService gradesPerLessonService;

    private SchoolClassRepository schoolClassRepository;

    private StudentMapper studentMapper = new StudentMapper();

    private SchoolClassService schoolClassService;

    @Autowired
    public StudentController(StudentRepository studentRepository, StudentService studentService,
                             GradesPerLessonRepository gradesPerLessonRepository,
                             SchoolLessonRepository schoolLessonRepository,
                             GradesPerLessonService gradesPerLessonService,
                             SchoolClassRepository schoolClassRepository,
                             SchoolClassService schoolClassService) {
        this.studentRepository = studentRepository;
        this.studentService = studentService;
        this.gradesPerLessonRepository = gradesPerLessonRepository;
        this.schoolLessonRepository = schoolLessonRepository;
        this.gradesPerLessonService = gradesPerLessonService;
        this.schoolClassRepository = schoolClassRepository;
        this.schoolClassService = schoolClassService;
    }

    @GetMapping(path = "api/students/get-all-per-class/{className}")
    public ResponseEntity<List<StudentDTO>> getAllStudentsDTO(@PathVariable String className) {
        List<StudentDTO> listOfStudentsDTO = studentService.getListOfStudentsDTO(className);
        return ResponseEntity.ok(listOfStudentsDTO);
    }

    @GetMapping(path = "api/student/get-student-by-class-and-number/{className}/{studentNumber}")
    public ResponseEntity<StudentWithGradesDTO> getStudentWithGrades(@PathVariable String className,
                                                                     @PathVariable int studentNumber) {

        Student student = studentRepository.findByNumberAndSchoolClass_NameIgnoreCase(studentNumber, className);
        StudentMapper studentMapper = new StudentMapper();
        StudentWithGradesDTO studentWithGradesDTO = studentMapper.mapStudentWithGrades(student);
        return ResponseEntity.ok(studentWithGradesDTO);
    }

    @GetMapping(path = "api/student/get-average/{studentId}/{lessonName}")
    public ResponseEntity<Float> getStudentAverageForSchoolLesson(@PathVariable UUID studentId,
                                                                  @PathVariable String lessonName) {
        Float average = studentService.getStudentAverage(studentId, lessonName);
        return ResponseEntity.ok(average);
    }

    @PutMapping(path = "api/student/add_grade")
    public void addStudentGrade(@Valid @RequestBody AddStudentGradeDTO addStudentGrade) {
        gradesPerLessonService.updateGrades(addStudentGrade);
    }

    @PostMapping(path = "api/student/add_new_student_to_class")
    public ResponseEntity<StudentDTO> addNewStudent(@Valid @RequestBody NewStudentDTO newStudentDTO) {
        StudentDTO studentDTO = studentService.addNewStudent(newStudentDTO);
        return ResponseEntity.ok(studentDTO);
    }

    @PutMapping(path = "api/student/move_student/{studentId}/{schoolClassId}")
    public void moveStudentToAnotherClass(@Valid @PathVariable @NotNull UUID studentId,
                                          @Valid @PathVariable @NotNull UUID schoolClassId) {
        studentService.moveStudentToAnotherClass(studentId, schoolClassId);
    }

    @DeleteMapping(path = "api/student/delete/{studentId}")
    public void deleteStudent(@Valid @PathVariable @NotNull UUID studentId) {
        Student foundedStudent = studentRepository.findById(studentId)
                .orElseThrow(() -> new EntityNotFoundException("The student does not exist: " + studentId));
        studentRepository.delete(foundedStudent);
    }

    @GetMapping(path = "api/student/get_all")
    public ResponseEntity<List<StudentDTO>> getListOfAllStudents() {
        List<StudentDTO> listOfStudentsDTO = studentService.getListOfAllStudents();
        return ResponseEntity.ok(listOfStudentsDTO);
    }

    @GetMapping(path = "api/student/count_all")
    public ResponseEntity<Long> countAllStudents() {
        Long numberOfAllStudents = studentService.getNumberOfAllStudents();
        return ResponseEntity.ok(numberOfAllStudents);
    }

    @GetMapping(path = "api/student/count_average_class_size")
    public ResponseEntity<Long> getAverageNumberOfStudentsInClasses() {
        Long average = studentService.getAverageNumberOfStudentsInClass();
        return ResponseEntity.ok(average);
    }

}
