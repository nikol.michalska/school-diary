package com.nikol.michalska.schoolDiary.controller;

import com.nikol.michalska.schoolDiary.model.entity.SchoolLesson;
import com.nikol.michalska.schoolDiary.repository.SchoolLessonRepository;
import com.nikol.michalska.schoolDiary.service.SchoolLessonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@RestController
public class SchoolLessonController {

    private SchoolLessonRepository schoolLessonRepository;

    private SchoolLessonService schoolLessonService;

    @Autowired
    public SchoolLessonController(SchoolLessonRepository schoolLessonRepository, SchoolLessonService schoolLessonService) {
        this.schoolLessonRepository = schoolLessonRepository;
        this.schoolLessonService = schoolLessonService;
    }

    @GetMapping(path = "api/schoolLesson/get-names-for-all")
    public ResponseEntity<List<String>> getNamesOfAllLessons() {
        List<String> listOfLessonsNames = schoolLessonService.getListOfSchoolLessonNames();
        return ResponseEntity.ok(listOfLessonsNames);
    }

    @DeleteMapping(path = "api/schoolLesson/delete_schoolLesson/{schoolLessonId}")
    public void deleteSchoolLesson(@PathVariable UUID schoolLessonId) {
        schoolLessonRepository.deleteById(schoolLessonId);
    }

    @PostMapping(path = "api/schoolLesson/create/{schoolLessonName}")
    public ResponseEntity<SchoolLesson> createNewSchoolLesson(@Valid @PathVariable @NotNull String schoolLessonName) {
        SchoolLesson savedSchoolLesson = schoolLessonService.createNewSchoolLesson(schoolLessonName);
        return ResponseEntity.ok(savedSchoolLesson);
    }
}
