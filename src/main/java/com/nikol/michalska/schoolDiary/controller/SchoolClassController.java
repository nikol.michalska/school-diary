package com.nikol.michalska.schoolDiary.controller;

import com.nikol.michalska.schoolDiary.model.DTO.SchoolClassAddStudentsDTO;
import com.nikol.michalska.schoolDiary.model.DTO.SchoolClassDTO;
import com.nikol.michalska.schoolDiary.model.entity.SchoolClass;
import com.nikol.michalska.schoolDiary.model.entity.Teacher;
import com.nikol.michalska.schoolDiary.model.mappers.SchoolClassMapper;
import com.nikol.michalska.schoolDiary.repository.SchoolClassRepository;
import com.nikol.michalska.schoolDiary.repository.StudentRepository;
import com.nikol.michalska.schoolDiary.repository.TeacherRepository;
import com.nikol.michalska.schoolDiary.service.SchoolClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@RestController
public class SchoolClassController {
    private SchoolClassRepository schoolClassRepository;

    private SchoolClassService schoolClassService;

    private TeacherRepository teacherRepository;

    private StudentRepository studentRepository;

    private SchoolClassMapper schoolClassMapper = new SchoolClassMapper();

    @Autowired
    public SchoolClassController(SchoolClassRepository schoolClassRepository,
                                 SchoolClassService schoolClassService,
                                 TeacherRepository teacherRepository,
                                 StudentRepository studentRepository) {
        this.schoolClassRepository = schoolClassRepository;
        this.schoolClassService = schoolClassService;
        this.teacherRepository = teacherRepository;
        this.studentRepository = studentRepository;
    }

    @GetMapping(path = "api/school-class/get-all-schoolclassDTO")
    public ResponseEntity<List<SchoolClassDTO>> getAllSchoolClassesDTO() {
        List<SchoolClassDTO> listOfSchoolClassesDTO = schoolClassService.getListOfSchoolClassDTO();
        return ResponseEntity.ok(listOfSchoolClassesDTO);
    }

    @PutMapping(path = "api/schoolClass/change_teacher/{schoolClassId}/{teacherId}")
    public ResponseEntity<SchoolClass> changeTeacher(@PathVariable UUID schoolClassId, @PathVariable UUID teacherId) {
        SchoolClass schoolClass = schoolClassRepository.findById(schoolClassId)
                .orElseThrow(() -> new EntityNotFoundException("There is no such school class"));
        Teacher teacher = teacherRepository.findById(teacherId)
                .orElseThrow(() -> new EntityNotFoundException("There is no such teacher"));
        schoolClass.setTeacher(teacher);
        SchoolClass savedSchoolClass = schoolClassRepository.save(schoolClass);
        return ResponseEntity.ok(savedSchoolClass);
    }

    @PostMapping(path = "api/schoolClass/create_new/{schoolClassName}")
    public ResponseEntity<SchoolClass> createNewSchoolClass(@Valid @PathVariable @NotNull String schoolClassName) {
        SchoolClass savedSchoolClass = schoolClassService.createNewSchoolClass(schoolClassName);
        return ResponseEntity.ok(savedSchoolClass);
    }

    @PutMapping(path = "api/schoolClass/add_students")
    public ResponseEntity<SchoolClassDTO> addStudents(@Valid @RequestBody SchoolClassAddStudentsDTO schoolClassAddStudentsDTO) {
        SchoolClassDTO schoolClassDTO = schoolClassService.assignStudentsToClass(schoolClassAddStudentsDTO);
        return ResponseEntity.ok(schoolClassDTO);
    }

    @GetMapping(path = "api/schoolClass/count_all")
    public ResponseEntity<Long> countAllSchoolClasses() {
       Long numberOfSchoolClasses = schoolClassService.getNumberOfAllSchoolClasses();
        return ResponseEntity.ok(numberOfSchoolClasses);
    }
}
