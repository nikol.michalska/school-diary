package com.nikol.michalska.schoolDiary.model.DTO;

import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@Setter
@Getter
public class NewTeacherDTO {

    @NotEmpty
    @Valid
    private String name;

    @NotEmpty
    @Valid
    private String lastName;

    @NotNull
    @Valid
    private List<UUID> schoolLessonsId;
}
