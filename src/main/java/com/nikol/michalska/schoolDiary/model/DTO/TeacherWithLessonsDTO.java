package com.nikol.michalska.schoolDiary.model.DTO;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Setter
@Getter
public class TeacherWithLessonsDTO {
    private UUID id;
    private String name;
    private String lastname;
    private List<String> lessonsName;
}
