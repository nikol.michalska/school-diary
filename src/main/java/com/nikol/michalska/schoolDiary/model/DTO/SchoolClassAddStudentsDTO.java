package com.nikol.michalska.schoolDiary.model.DTO;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.UUID;

@Setter
@Getter
public class SchoolClassAddStudentsDTO {

    @NotNull
    private UUID schoolClassId;

    @NotNull
    @Size(min = 1)
    private List<UUID> listOfStudents;


}
