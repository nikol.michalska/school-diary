package com.nikol.michalska.schoolDiary.model.DTO;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.UUID;

@Setter
@Getter
public class NewStudentDTO {

    @Valid
    @NotEmpty
    private String name;

    @Valid
    @NotEmpty
    private String lastName;

    @Valid
    @NotNull
    private UUID schoolClassId;

}
