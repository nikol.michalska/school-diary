package com.nikol.michalska.schoolDiary.model.mappers;

import com.nikol.michalska.schoolDiary.model.DTO.GradesPerLessonDTO;
import com.nikol.michalska.schoolDiary.model.entity.GradesPerLesson;

public class GradesPerLessonMapper {

    public GradesPerLessonDTO mapGradesPerLesson(GradesPerLesson gradesPerLesson){
        GradesPerLessonDTO gradesPerLessonDTO = new GradesPerLessonDTO();
        gradesPerLessonDTO.setGrades(gradesPerLesson.getGrades());
        gradesPerLessonDTO.setLessonName(gradesPerLesson.getSchoolLesson().getName());
        return gradesPerLessonDTO;
    }
}
