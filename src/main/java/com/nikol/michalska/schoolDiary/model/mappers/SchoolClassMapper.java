package com.nikol.michalska.schoolDiary.model.mappers;

import com.nikol.michalska.schoolDiary.model.DTO.SchoolClassDTO;
import com.nikol.michalska.schoolDiary.model.DTO.TeacherDTO;
import com.nikol.michalska.schoolDiary.model.entity.SchoolClass;

public class SchoolClassMapper {

    public SchoolClassDTO mapSchoolClass(SchoolClass schoolClass) {
        SchoolClassDTO schoolClassDTO = new SchoolClassDTO();
        schoolClassDTO.setId(schoolClass.getId());
        schoolClassDTO.setName(schoolClass.getName());

        if(schoolClass.getListOfStudents() == null){
            schoolClassDTO.setNumberOfStudents(0);
        } else {
            schoolClassDTO.setNumberOfStudents(schoolClass.getListOfStudents().size());
        }

        TeacherMapper teacherMapper = new TeacherMapper();
        if(schoolClass.getTeacher() != null) {
            TeacherDTO teacherDTO = teacherMapper.mapTeacher(schoolClass.getTeacher());
            schoolClassDTO.setTeacherDTO(teacherDTO);
        }
        return schoolClassDTO;
    }
}
