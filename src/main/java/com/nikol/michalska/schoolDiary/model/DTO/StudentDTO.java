package com.nikol.michalska.schoolDiary.model.DTO;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Setter
@Getter
public class StudentDTO {
    private UUID id;
    private String name;
    private String lastname;

}
