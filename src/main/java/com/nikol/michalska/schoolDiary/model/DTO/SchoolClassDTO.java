package com.nikol.michalska.schoolDiary.model.DTO;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Setter
@Getter
public class SchoolClassDTO {
    private UUID id;
    private String name;
    private TeacherDTO teacherDTO;
    private int numberOfStudents;
}
