package com.nikol.michalska.schoolDiary.model.DTO;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class StudentWithGradesDTO {
    private String firstName;
    private String lastname;
    private int number;
    private List<GradesPerLessonDTO> grades;

}
