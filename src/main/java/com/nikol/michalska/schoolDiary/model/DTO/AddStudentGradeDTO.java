package com.nikol.michalska.schoolDiary.model.DTO;

import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Setter
@Getter
public class AddStudentGradeDTO {

    @NotNull
    @Valid
    private UUID studentId;

    @NotNull
    @Valid
    private UUID schoolLessonId;

    @NotEmpty
    @Valid
    private String grade;
}
