package com.nikol.michalska.schoolDiary.model.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Setter
@Getter
public class Student {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "pg-uuid")
    private UUID id;

    private String name;
    private  String lastname;
    private int number;

    @ManyToOne
    private SchoolClass schoolClass;

    @OneToMany
    List<GradesPerLesson> listOfGrades;
}

