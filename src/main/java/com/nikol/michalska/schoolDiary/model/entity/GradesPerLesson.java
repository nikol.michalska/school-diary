package com.nikol.michalska.schoolDiary.model.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Setter
@Getter
public class GradesPerLesson implements Serializable {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "pg-uuid")
    private UUID id;

    @ManyToOne
    private Student student;

    @ManyToOne
    private SchoolLesson schoolLesson;

    private String grades;

    public void addGrade(String grade){
        grades = grades + " " + grade;
    }

}
