package com.nikol.michalska.schoolDiary.model.DTO;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class GradesPerLessonDTO {

    private String lessonName;
    private String grades;

}
