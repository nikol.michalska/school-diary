package com.nikol.michalska.schoolDiary.model.DTO;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.util.UUID;

@Setter
@Getter
public class TeacherDTO {
    private UUID id;

    @NotEmpty
    private String name;

    @NotEmpty
    private String lastname;
}
