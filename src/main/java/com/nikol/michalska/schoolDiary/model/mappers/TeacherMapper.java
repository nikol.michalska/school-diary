package com.nikol.michalska.schoolDiary.model.mappers;

import com.nikol.michalska.schoolDiary.model.DTO.TeacherDTO;
import com.nikol.michalska.schoolDiary.model.DTO.TeacherWithLessonsDTO;
import com.nikol.michalska.schoolDiary.model.entity.Teacher;

import java.util.ArrayList;
import java.util.List;

public class TeacherMapper {

    public TeacherDTO mapTeacher(Teacher teacher) {
        TeacherDTO teacherDTO = new TeacherDTO();
        teacherDTO.setId(teacher.getId());
        teacherDTO.setName(teacher.getName());
        teacherDTO.setLastname(teacher.getLastname());
        return teacherDTO;
    }

    public TeacherWithLessonsDTO mapTeacherWithLessons(Teacher teacher) {
        TeacherWithLessonsDTO teacherWithLessonsDTO = new TeacherWithLessonsDTO();
        teacherWithLessonsDTO.setId(teacher.getId());
        teacherWithLessonsDTO.setName(teacher.getName());
        teacherWithLessonsDTO.setLastname(teacher.getLastname());
        List<String> listOfLessons = new ArrayList<>();

        if (teacher.getListOfSchoolLessons() != null) {
            for (int a = 0; a < teacher.getListOfSchoolLessons().size(); a++) {
                String lessonName = teacher.getListOfSchoolLessons().get(a).getName();
                listOfLessons.add(lessonName);
            }
        }

        teacherWithLessonsDTO.setLessonsName(listOfLessons);
        return teacherWithLessonsDTO;
    }
}
