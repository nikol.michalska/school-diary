package com.nikol.michalska.schoolDiary.model.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

@Entity
@Setter
@Getter
public class SchoolClass {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "pg-uuid")
    private UUID id;
    private String name;

    @OneToMany
    private List<Student> listOfStudents = new ArrayList<>();

    @OneToOne
    private Teacher teacher;

    public void assignStudentNumbers(){
        getListOfStudents().sort(Comparator.comparing(Student::getLastname));
        for (int a = 0; a < getListOfStudents().size(); a++) {
            getListOfStudents().get(a).setNumber(a + 1);
        }
    }
}
