package com.nikol.michalska.schoolDiary.model.entity;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.UUID;

@Entity
@Setter
@Getter
public class Teacher {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "pg-uuid")
    private UUID id;

    @NotEmpty(message = "You must provide name")
    @Valid
    private String name;

    @NotEmpty(message = "You must provide name")
    @Valid
    private String lastname;

    @OneToMany
    private List<SchoolLesson> listOfSchoolLessons;
}
