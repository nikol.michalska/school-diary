package com.nikol.michalska.schoolDiary.model.mappers;

import com.nikol.michalska.schoolDiary.model.DTO.GradesPerLessonDTO;
import com.nikol.michalska.schoolDiary.model.DTO.StudentDTO;
import com.nikol.michalska.schoolDiary.model.DTO.StudentWithGradesDTO;
import com.nikol.michalska.schoolDiary.model.entity.GradesPerLesson;
import com.nikol.michalska.schoolDiary.model.entity.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentMapper {

    public StudentDTO mapStudent(Student student) {
        StudentDTO studentDTO = new StudentDTO();
        studentDTO.setId(student.getId());
        studentDTO.setName(student.getName());
        studentDTO.setLastname(student.getLastname());
        return studentDTO;
    }

    public StudentWithGradesDTO mapStudentWithGrades(Student student) {
        GradesPerLessonMapper gradesPerLessonMapper = new GradesPerLessonMapper();
        StudentWithGradesDTO studentWithGradesDTO = new StudentWithGradesDTO();
        studentWithGradesDTO.setFirstName(student.getName());
        studentWithGradesDTO.setLastname(student.getLastname());
        studentWithGradesDTO.setNumber(student.getNumber());

        List<GradesPerLessonDTO> gradesPerLessonDTOList = new ArrayList<>();
        if (student.getListOfGrades() != null) {
            for (int a = 0; a < student.getListOfGrades().size(); a++) {
                GradesPerLesson gradesPerLesson = student.getListOfGrades().get(a);
                GradesPerLessonDTO gradesPerLessonDTO = gradesPerLessonMapper.mapGradesPerLesson(gradesPerLesson);
                gradesPerLessonDTOList.add(gradesPerLessonDTO);
            }

        }


        studentWithGradesDTO.setGrades(gradesPerLessonDTOList);
        return studentWithGradesDTO;
    }

    public List<StudentDTO> mapStudents(List<Student> listOfStudents){
        List<StudentDTO> listOfStudentsDTO = new ArrayList<>();
        for (int a = 0; a < listOfStudents.size(); a++) {
            StudentDTO studentDTO = mapStudent(listOfStudents.get(a));
            listOfStudentsDTO.add(studentDTO);
        }
        return listOfStudentsDTO;
    }
}
