INSERT INTO teacher(id, name, lastname)
VALUES ('fe46ad55-a581-414f-9512-94eb34718916', 'John', 'Newman');

INSERT INTO teacher(id, name, lastname)
VALUES ('a141d324-6dde-40cd-a0c8-d283eb3c081d', 'Alice', 'Crown');

INSERT INTO teacher(id, name, lastname)
VALUES ('366e2124-0354-41b1-9223-945d7c2ecd61', 'Chris', 'Martin');

INSERT INTO school_class(id, name, teacher_id)
VALUES ('05c55fb3-f7a9-4cea-ba19-ba0e6132bb46', '2C', 'fe46ad55-a581-414f-9512-94eb34718916');

INSERT INTO school_class(id, name, teacher_id)
VALUES ('d3444891-6461-48ce-8288-299afcb87b27', '1A', 'a141d324-6dde-40cd-a0c8-d283eb3c081d');

INSERT INTO student(id, lastname, name, school_class_id, number)
VALUES ('5a477773-ac08-4904-b0d4-a6ad0c180188', 'Brown', 'Peter', '05c55fb3-f7a9-4cea-ba19-ba0e6132bb46', 12);

INSERT INTO student(id, lastname, name, school_class_id, number)
VALUES ('201f71e2-981c-4d99-8b36-1f33ac8b7689', 'Door', 'Sophie', '05c55fb3-f7a9-4cea-ba19-ba0e6132bb46', 11);

INSERT INTO student(id, lastname, name, school_class_id, number)
VALUES ('eec678b3-c9f7-44b9-8b2b-bb207e08d927', 'Fly', 'Henry', '05c55fb3-f7a9-4cea-ba19-ba0e6132bb46', 10);

INSERT INTO student(id, lastname, name, school_class_id, number)
VALUES ('d6b6cdae-4a40-4b98-bda3-cc2000f36056', 'Mince', 'Melanie', 'd3444891-6461-48ce-8288-299afcb87b27', 1);

INSERT INTO student(id, lastname, name, school_class_id, number)
VALUES ('39de3ada-a24a-4440-955e-2a6e48da6cc4', 'Field', 'Alexy', 'd3444891-6461-48ce-8288-299afcb87b27', 2);

INSERT INTO student(id, lastname, name, school_class_id, number)
VALUES ('5e23b24c-1bd4-40cf-9628-ed4eb996060d', 'Thomson', 'Emily', 'd3444891-6461-48ce-8288-299afcb87b27', 3);


INSERT INTO school_class_list_of_students(school_class_id, list_of_students_id)
VALUES ('05c55fb3-f7a9-4cea-ba19-ba0e6132bb46', '201f71e2-981c-4d99-8b36-1f33ac8b7689');

INSERT INTO school_class_list_of_students(school_class_id, list_of_students_id)
VALUES ('05c55fb3-f7a9-4cea-ba19-ba0e6132bb46', '5a477773-ac08-4904-b0d4-a6ad0c180188');

INSERT INTO school_class_list_of_students(school_class_id, list_of_students_id)
VALUES ('05c55fb3-f7a9-4cea-ba19-ba0e6132bb46', 'eec678b3-c9f7-44b9-8b2b-bb207e08d927');



INSERT INTO school_lesson(id, name, teacher_id)
VALUES ('1c89dbcf-1319-4669-ad13-1c479e2dd94a', 'PE', 'fe46ad55-a581-414f-9512-94eb34718916');

INSERT INTO school_lesson(id, name, teacher_id)
VALUES ('9b85aeac-ddaf-4dfe-8be0-bb14d8eb4319', 'History', 'a141d324-6dde-40cd-a0c8-d283eb3c081d');

INSERT INTO teacher_list_of_school_lessons(teacher_id, list_of_school_lessons_id)
VALUES  ('fe46ad55-a581-414f-9512-94eb34718916', '1c89dbcf-1319-4669-ad13-1c479e2dd94a');


INSERT INTO grades_per_lesson(id, grades, student_id, school_lesson_id)
VALUES  ('ff6aa609-60d4-40ea-9fd1-05c8210faab9', '4 4 5 5 3', '5a477773-ac08-4904-b0d4-a6ad0c180188', '1c89dbcf-1319-4669-ad13-1c479e2dd94a');

INSERT INTO student_list_of_grades(student_id, list_of_grades_id)
VALUES ('5a477773-ac08-4904-b0d4-a6ad0c180188', 'ff6aa609-60d4-40ea-9fd1-05c8210faab9');