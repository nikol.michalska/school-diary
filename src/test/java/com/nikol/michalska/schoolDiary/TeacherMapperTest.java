package com.nikol.michalska.schoolDiary;

import com.nikol.michalska.schoolDiary.model.DTO.TeacherDTO;
import com.nikol.michalska.schoolDiary.model.DTO.TeacherWithLessonsDTO;
import com.nikol.michalska.schoolDiary.model.entity.SchoolLesson;
import com.nikol.michalska.schoolDiary.model.entity.Teacher;
import com.nikol.michalska.schoolDiary.model.mappers.TeacherMapper;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TeacherMapperTest {

    @Test
    public void mapTeacherTest() {
        Teacher teacher = getTeacher();

        TeacherMapper teacherMapper = new TeacherMapper();
        TeacherDTO teacherDTO = teacherMapper.mapTeacher(teacher);
        assertEquals(teacher.getId(), teacherDTO.getId());
        assertEquals(teacher.getName(), teacherDTO.getName());
        assertEquals(teacher.getLastname(), teacherDTO.getLastname());

    }

    @Test
    public void mapTeacherWithLessonsTest_if_has_no_lessons() {
        Teacher teacher = getTeacher();

        TeacherMapper teacherMapper = new TeacherMapper();
        TeacherWithLessonsDTO teacherWithLessonsDTO = teacherMapper.mapTeacherWithLessons(teacher);
        assertEquals(teacher.getId(), teacherWithLessonsDTO.getId());
        assertEquals(teacher.getName(), teacherWithLessonsDTO.getName());
        assertEquals(teacher.getLastname(), teacherWithLessonsDTO.getLastname());

    }

    @Test
    public void mapTeacherWithLessonsTest_if_has_lessons() {
        Teacher teacher = getTeacher();

        TeacherMapper teacherMapper = new TeacherMapper();
        SchoolLesson schoolLesson = new SchoolLesson();
        schoolLesson.setName("PE");

        List<SchoolLesson> schoolLessonList = new ArrayList<>();
        schoolLessonList.add(schoolLesson);
        teacher.setListOfSchoolLessons(schoolLessonList);


        TeacherWithLessonsDTO teacherWithLessonsDTO = teacherMapper.mapTeacherWithLessons(teacher);

        assertEquals(teacher.getId(), teacherWithLessonsDTO.getId());
        assertEquals(teacher.getName(), teacherWithLessonsDTO.getName());
        assertEquals(teacher.getLastname(), teacherWithLessonsDTO.getLastname());
        assertEquals(teacher.getListOfSchoolLessons().get(0).getName(), teacherWithLessonsDTO.getLessonsName().get(0));
        assertEquals(teacher.getListOfSchoolLessons().size(), teacherWithLessonsDTO.getLessonsName().size());


    }

    private Teacher getTeacher() {
        Teacher teacher = new Teacher();
        teacher.setId(UUID.fromString("59acd49c-c975-4516-9015-4407f05e6d8b"));
        teacher.setName("Alex");
        teacher.setLastname("Fox");
        return teacher;
    }
}
