package com.nikol.michalska.schoolDiary;


import com.nikol.michalska.schoolDiary.model.DTO.SchoolClassDTO;
import com.nikol.michalska.schoolDiary.model.entity.SchoolClass;
import com.nikol.michalska.schoolDiary.repository.SchoolClassRepository;
import com.nikol.michalska.schoolDiary.repository.StudentRepository;
import com.nikol.michalska.schoolDiary.service.SchoolClassService;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class SchoolClassServiceTest {

    @Test
    public void getListOfSchoolClassDTO_Test_if_correct_list_is_returned() {
        SchoolClassRepository schoolClassRepository = mock(SchoolClassRepository.class);

        SchoolClass schoolClass1 = getSchoolClass("1C");
        SchoolClass schoolClass2 = getSchoolClass("3D");
        List<SchoolClass> schoolClasses = new ArrayList<>();
        schoolClasses.add(schoolClass1);
        schoolClasses.add(schoolClass2);

        when(schoolClassRepository.findAll()).thenReturn(schoolClasses);

        StudentRepository studentRepository = mock(StudentRepository.class);

        SchoolClassService schoolClassService = new SchoolClassService(schoolClassRepository, studentRepository);

        List<SchoolClassDTO> schoolClassDTOS = schoolClassService.getListOfSchoolClassDTO();

        assertEquals(schoolClassDTOS.size(), 2);
        assertEquals(schoolClassDTOS.get(0).getName(), "1C");
        assertEquals(schoolClassDTOS.get(1).getName(), "3D");
    }


    private SchoolClass getSchoolClass(String name) {
        SchoolClass schoolClass = new SchoolClass();
        schoolClass.setName(name);
        return schoolClass;
    }
}
