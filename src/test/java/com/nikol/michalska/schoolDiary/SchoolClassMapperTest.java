package com.nikol.michalska.schoolDiary;

import com.nikol.michalska.schoolDiary.model.DTO.SchoolClassDTO;
import com.nikol.michalska.schoolDiary.model.entity.SchoolClass;
import com.nikol.michalska.schoolDiary.model.entity.Student;
import com.nikol.michalska.schoolDiary.model.mappers.SchoolClassMapper;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SchoolClassMapperTest {

    @Test
    public void mapSchoolClassTest_no_students() {
        SchoolClass schoolClass = getSchoolClass();
        SchoolClassMapper schoolClassMapper = new SchoolClassMapper();
        SchoolClassDTO schoolClassDTO = schoolClassMapper.mapSchoolClass(schoolClass);

        assertEquals(schoolClass.getName(), schoolClassDTO.getName());
        assertEquals(schoolClass.getId(), schoolClassDTO.getId());
        assertEquals(0, schoolClassDTO.getNumberOfStudents());


    }

    @Test
    public void mapSchoolClassTest_with_students() {
        SchoolClass schoolClass = getSchoolClass();
        SchoolClassMapper schoolClassMapper = new SchoolClassMapper();

        Student student = new Student();

        List<Student> studentList = new ArrayList<>();
        studentList.add(student);
        schoolClass.setListOfStudents(studentList);

        SchoolClassDTO schoolClassDTO = schoolClassMapper.mapSchoolClass(schoolClass);

        assertEquals(schoolClass.getName(), schoolClassDTO.getName());
        assertEquals(schoolClass.getId(), schoolClassDTO.getId());
        assertEquals(1, schoolClassDTO.getNumberOfStudents());

    }

    private SchoolClass getSchoolClass() {
        SchoolClass schoolClass = new SchoolClass();

        schoolClass.setId(UUID.fromString("f38ebc9a-c915-424c-9009-ec15cac1abc5"));
        schoolClass.setName("1C");
        return schoolClass;
    }
}
