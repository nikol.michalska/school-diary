package com.nikol.michalska.schoolDiary;

import com.nikol.michalska.schoolDiary.repository.GradesPerLessonRepository;
import com.nikol.michalska.schoolDiary.repository.SchoolClassRepository;
import com.nikol.michalska.schoolDiary.repository.StudentRepository;
import com.nikol.michalska.schoolDiary.service.SchoolClassService;
import com.nikol.michalska.schoolDiary.service.StudentService;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StudentServiceTest {

    @Test
    public void getAverageNumberOfStudentsInClassTest() {
        StudentRepository studentRepository = mock(StudentRepository.class);
        Long numberOfStudents = 6L;
        when(studentRepository.count()).thenReturn(numberOfStudents);


        SchoolClassRepository schoolClassRepository = mock(SchoolClassRepository.class);
        Long numberOfSchoolClasses = 2L;
        when((schoolClassRepository.count())).thenReturn(numberOfSchoolClasses);


        GradesPerLessonRepository gradesPerLessonRepository = mock((GradesPerLessonRepository.class));
        SchoolClassService schoolClassService = new SchoolClassService(schoolClassRepository, studentRepository);


        StudentService studentService = new StudentService(studentRepository,
                gradesPerLessonRepository, schoolClassRepository, schoolClassService);

        Long average = studentService.getAverageNumberOfStudentsInClass();
        assertEquals(average, 3);
    }
}
