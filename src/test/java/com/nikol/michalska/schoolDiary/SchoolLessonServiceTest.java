package com.nikol.michalska.schoolDiary;

import com.nikol.michalska.schoolDiary.model.entity.SchoolLesson;
import com.nikol.michalska.schoolDiary.repository.SchoolLessonRepository;
import com.nikol.michalska.schoolDiary.service.SchoolLessonService;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SchoolLessonServiceTest {

    @Test
    public void getListOfSchoolLessonNamesTest() {
        SchoolLessonRepository schoolLessonRepository = mock(SchoolLessonRepository.class);
        SchoolLesson schoolLesson = new SchoolLesson();
        schoolLesson.setName("PE");
        List<SchoolLesson> schoolLessonList = new ArrayList<>();
        schoolLessonList.add(schoolLesson);
        when(schoolLessonRepository.findAll()).thenReturn(schoolLessonList);

        SchoolLessonService schoolLessonService = new SchoolLessonService(schoolLessonRepository);
        List<String> schoolLessonNames = schoolLessonService.getListOfSchoolLessonNames();

        assertEquals("PE", schoolLessonNames.get(0));
        assertEquals(1, schoolLessonNames.size());
    }


}
