package com.nikol.michalska.schoolDiary;

import com.nikol.michalska.schoolDiary.model.DTO.GradesPerLessonDTO;
import com.nikol.michalska.schoolDiary.model.entity.GradesPerLesson;
import com.nikol.michalska.schoolDiary.model.entity.SchoolLesson;
import com.nikol.michalska.schoolDiary.model.mappers.GradesPerLessonMapper;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GradesPerLessonMapperTest {

    @Test
    public void mapGradesPerLessonTest() {
        GradesPerLesson gradesPerLesson = getGradesPerLesson();
        GradesPerLessonMapper gradesPerLessonMapper = new GradesPerLessonMapper();
        GradesPerLessonDTO gradesPerLessonDTO = gradesPerLessonMapper.mapGradesPerLesson(gradesPerLesson);

        assertEquals(gradesPerLesson.getGrades(), gradesPerLessonDTO.getGrades());
        assertEquals(gradesPerLesson.getSchoolLesson().getName(), gradesPerLessonDTO.getLessonName());
    }

    private GradesPerLesson getGradesPerLesson() {
        GradesPerLesson gradesPerLesson = new GradesPerLesson();
        gradesPerLesson.setGrades("3 5 5");

        SchoolLesson schoolLesson = new SchoolLesson();
        schoolLesson.setName("PE");
        gradesPerLesson.setSchoolLesson(schoolLesson);
        return gradesPerLesson;
    }
}
