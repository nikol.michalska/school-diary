package com.nikol.michalska.schoolDiary;

import com.nikol.michalska.schoolDiary.model.entity.SchoolClass;
import com.nikol.michalska.schoolDiary.model.entity.Student;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SchoolClassTest {

    @Test
    public void assignStudentNumbersTest() {
        SchoolClass schoolClass = new SchoolClass();
        Student student1 = getStudent("Brown", 4);
        Student student2 = getStudent("Zulu", 9);
        schoolClass.setListOfStudents(Arrays.asList(student1, student2));
        schoolClass.assignStudentNumbers();

        assertEquals(student1.getNumber(), 1);
        assertEquals(student2.getNumber(), 2);
    }

    public Student getStudent(String lastname, int number) {
        Student student = new Student();
        student.setLastname(lastname);
        student.setNumber(number);
        return student;
    }

}
