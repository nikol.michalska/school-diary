package com.nikol.michalska.schoolDiary;

import com.nikol.michalska.schoolDiary.model.DTO.StudentDTO;
import com.nikol.michalska.schoolDiary.model.DTO.StudentWithGradesDTO;
import com.nikol.michalska.schoolDiary.model.entity.GradesPerLesson;
import com.nikol.michalska.schoolDiary.model.entity.SchoolLesson;
import com.nikol.michalska.schoolDiary.model.entity.Student;
import com.nikol.michalska.schoolDiary.model.mappers.StudentMapper;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StudentMapperTest {

    @Test
    public void mapStudentTest() {
        Student student = getStudent();

        StudentMapper studentMapper = new StudentMapper();
        StudentDTO studentDTO = studentMapper.mapStudent(student);

        assertEquals(student.getId(), studentDTO.getId());
        assertEquals(student.getName(), studentDTO.getName());
        assertEquals(student.getLastname(), studentDTO.getLastname());
    }

    @Test
    public void mapStudentWithGradesTest_if_no_grades() {
        Student student = getStudent();
        StudentMapper studentMapper = new StudentMapper();
        StudentWithGradesDTO studentWithGradesDTO = studentMapper.mapStudentWithGrades(student);

        assertEquals(student.getName(), studentWithGradesDTO.getFirstName());
        assertEquals(student.getLastname(), studentWithGradesDTO.getLastname());
    }

    @Test
    public void mapStudentWithGradesTest_with_grades() {
        Student student = getStudent();

        List<GradesPerLesson> listOfGrades = new ArrayList<>();

        SchoolLesson schoolLesson = new SchoolLesson();
        schoolLesson.setName("Music");

        GradesPerLesson gradesPerLesson = new GradesPerLesson();
        gradesPerLesson.setGrades("3 5 2 6");
        gradesPerLesson.setSchoolLesson(schoolLesson);
        listOfGrades.add(gradesPerLesson);

        student.setListOfGrades(listOfGrades);

        StudentMapper studentMapper = new StudentMapper();
        StudentWithGradesDTO studentWithGradesDTO = studentMapper.mapStudentWithGrades(student);

        assertEquals(student.getName(), studentWithGradesDTO.getFirstName());
        assertEquals(student.getLastname(), studentWithGradesDTO.getLastname());
        assertEquals(student.getListOfGrades().size(), studentWithGradesDTO.getGrades().size());
        assertEquals(student.getListOfGrades().get(0).getGrades(), studentWithGradesDTO.getGrades().get(0).getGrades());

    }

    @Test
    public void mapsStudentsTest() {
        Student student = getStudent();
        List<Student> listOfStudents = new ArrayList<>();
        listOfStudents.add(student);


        StudentMapper studentMapper = new StudentMapper();
        List<StudentDTO> listOfStudentsDTO = studentMapper.mapStudents(listOfStudents);

        assertEquals(listOfStudents.get(0).getId(), listOfStudentsDTO.get(0).getId());
        assertEquals(listOfStudents.get(0).getName(), listOfStudentsDTO.get(0).getName());
        assertEquals(listOfStudents.get(0).getLastname(), listOfStudentsDTO.get(0).getLastname());


    }

    private Student getStudent() {
        Student student = new Student();
        student.setId(UUID.fromString("b050253f-ef32-4df1-84cd-47a57f861319"));
        student.setName("Adam");
        student.setLastname("Brown");
        return student;
    }
}
