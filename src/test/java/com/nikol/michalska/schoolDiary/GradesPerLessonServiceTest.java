package com.nikol.michalska.schoolDiary;

import com.nikol.michalska.schoolDiary.model.entity.GradesPerLesson;
import com.nikol.michalska.schoolDiary.model.entity.SchoolLesson;
import com.nikol.michalska.schoolDiary.model.entity.Student;
import com.nikol.michalska.schoolDiary.repository.GradesPerLessonRepository;
import com.nikol.michalska.schoolDiary.repository.SchoolLessonRepository;
import com.nikol.michalska.schoolDiary.repository.StudentRepository;
import com.nikol.michalska.schoolDiary.service.GradesPerLessonService;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GradesPerLessonServiceTest {

    @Test
    public void createNewGradesPerLessonTest() {
        StudentRepository studentRepository = mock(StudentRepository.class);
        Student student = new Student();
        student.setId(UUID.fromString("55be4397-c8e9-4284-8e55-01a07990fdc4"));
        when(studentRepository.findByIdOrThrow(UUID.fromString("55be4397-c8e9-4284-8e55-01a07990fdc4")))
                .thenReturn(student);

        SchoolLessonRepository schoolLessonRepository = mock(SchoolLessonRepository.class);
        SchoolLesson schoolLesson = new SchoolLesson();
        schoolLesson.setId(UUID.fromString("2ef7b493-d864-4425-93dd-f6b5fe929f6d"));
        when(schoolLessonRepository.findByIdOrThrow(UUID.fromString("2ef7b493-d864-4425-93dd-f6b5fe929f6d")))
                .thenReturn(schoolLesson);


        GradesPerLessonRepository gradesPerLessonRepository = mock((GradesPerLessonRepository.class));

        GradesPerLessonService gradesPerLessonService = new GradesPerLessonService
                (studentRepository, schoolLessonRepository, gradesPerLessonRepository);
        GradesPerLesson gradesPerLesson = gradesPerLessonService.createNewGradesPerLesson(student.getId(), schoolLesson.getId());

        assertEquals(gradesPerLesson.getGrades(), "");
        assertEquals(gradesPerLesson.getStudent(), student);
        assertEquals(gradesPerLesson.getSchoolLesson(), schoolLesson);
    }
}
