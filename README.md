Project description:
-
It is my first Spring project, created for learning purpose. It provides endpoints that perfom all CRUD operations on database. Project simulates school application that stores information about teachers, classes, students, their grades and enables to change this data. 

Technologies used:
-
- Java 17
- Spring
- Hibernate
- PostgreSQL
- JUnit
- Mockito
- Swagger
- Maven
- Postman (For testing endpoints)